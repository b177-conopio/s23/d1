// select a database
use <database name>

// when creating a new database via the comman line, the use command can be entered with a name of database that does not exist yet. Once a new record is inserted into the database, the database will be created. 

// database = filing cabinet
// collection = drawer
// document/record = folder
// sub-documents (optional)
// fields = file/folder content

// Insert one document (create)

db.users.insert({
	firstname: "Jane",
	lastName: "Doe",
	age: 21,
	contact: {
		phone: "123456789",
		email: "janedoe@mail.com"
	},
	courses: ["CSS", "Javascript", "Python"],
	department: "none"
})

// if inserting/creating a new document within a collection that does not exist, MonogDB will automatically create that collection

// insert many
db.users.insertMany([
	{
		firstName: "Stephen",
		lastName: "Hawking",
		age: 76,
		contact: {
			phone: "123456789",
			email: "stephenhawking@mail.com"
		},
		courses: ["Python", "React", "PHP"],
		department: "none"
	},
	{
		firstName: "Neil",
		lastName: "Armstrong",
		age: 82,
		contact: {
			phone: "123456789",
			email: "neilarmstrong@mail.com"
		},
		courses: ["React", "Laravel", "SASS"],
		department: "none"
	}
])

// Finding documents (Read)
// retrieve a list of all users
db.users.find()

// Find a specific user
db.users.find({firstName: "Stephen"})

// find documents with multiple conditions
db.users.find({lastName:"Armstrong", age:82})

// Update/Edit Documents


// Create
db.users.insert({
	firstName: "Test",
	lastName: "Test",
	age: 0,
	contact: {
		phone: "123456789",
		email: "test@mail.com"
	},
	courses: [],
	department: "none"
})

// updateOne always needs to be provided two objects. The first object specifies which document needs to update. The second object contains the $set operator are the specific fields to be updated
db.users.updateOne(
	{_id: ObjectId("62876c46ff9c725a7d3d546d")},
	{
		$set: {
			firstName: "Bill",
			lastName: "Gates",
			age: 65,
			contact: {
				phone: "123456789",
				email: "billgates@mail.com"
			},
			courses: ["PHP", "Laravel", "HTML"],
			department: "Operations"
		}
	})

// update multiple documents
db.users.updateMany(
	{department: "none"},
	{
		$set: {
			department: "HR"
		}
	})

// Deleting document

// creating a document to delete
db.users.insert({
	firstName:"test"
})

// deleting a single document
db.users.deleteOne({
	firstName:"test"
})

// deleting many records
// Be careful when using deleteMany. If no search criteria are provided, it will delete ALL documents within the given collection

// create another user to delete
db.users.insert({
	firstName: "Bill"
})

// delete many
db.users.deleteMany({
	firstName:"Bill"
})